var kpiob12_sid = '', UserIP = null, ipInfo = null, intelligent_solutions = 0, intro = 0, modules = 0, gallery = 0, about_us = 0, req_demo = 0;
var date = new Date();
var date1 = (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getFullYear();
var hours = date.getHours();
var minutes = date.getMinutes();
var ampm = hours >= 12 ? 'pm' : 'am';
hours = hours % 12;
hours = hours ? hours : 12; // the hour '0' should be '12'
minutes = minutes < 10 ? '0' + minutes : minutes;
var strTime = date1 + ' - ' + hours + ':' + minutes + ' ' + ampm;
var cTimeZone = date.toString().split('(');
cTimeZone = cTimeZone[1].toString().split(')');
cTimeZone = cTimeZone[0];
$(document).ready(function () {

  $(".close").click(function () {
    if ($(".modal").hasClass("fade")) {
      document.getElementById('video_tag').pause();
    } else {
      // document.getElementById('video_tag').play();
    }
  });

  $("body").click(function () {
    if ($(".modal").hasClass("fade")) {
      document.getElementById('video_tag').pause();
    } else {
      // document.getElementById('video_tag').play();
    }
  });


  document.onkeydown = function (evt) {
    if (evt.key == 'Escape') {
      document.getElementById('video_tag').pause();
    } else {
      // document.getElementById('video_tag').play();
    }
  };


  $(".quick-links span, button.demo-btn").click(function () {
    var scrollTo = $(this).attr('data-scroll')
    $('html,body').animate({
      scrollTop: $('#' + scrollTo).offset().top - 100
    },
      'slow');
    cookieSetter(scrollTo);
  });
  $("ul.navbar-nav li").click(function () {
    if ($("#mySidenav")) {
      closeNav();
    }
    var scrollTo = $(this).find('a').attr('data-link');
    $('html,body').animate({
      scrollTop: $('#' + scrollTo).offset().top - 100
    },
      'slow');
    cookieSetter(scrollTo);

  });

  $num = $('.my-card').length;
  $even = $num / 2;
  $odd = ($num + 1) / 2;

  if ($num % 2 == 0) {
    $('.my-card:nth-child(' + $even + ')').addClass('active');
    $('.my-card:nth-child(' + $even + ')').prev().addClass('prev');
    $('.my-card:nth-child(' + $even + ')').next().addClass('next');
  } else {
    $('.my-card:nth-child(' + $odd + ')').addClass('active');
    $('.my-card:nth-child(' + $odd + ')').prev().addClass('prev');
    $('.my-card:nth-child(' + $odd + ')').next().addClass('next');
  }





  var v = document.cookie.match('(^|;) ?kpiob12.sid=([^;]*)(;|$)');
  (v && v[2]) ? kpiob12_sid = v[2] : '';
  $.getJSON('http://ip-api.com/json', function (data) {
    UserIP = data.query;
    data.time = strTime;
    data.cTimeZone = cTimeZone;
    ipInfo = data;
    data.piob12og9 = kpiob12_sid;
    console.log(data)
    // $.post("/visit",data, function(data){
    //     document.cookie = "kpiob12.sid=" + data;
    //   });
  });
});


function cookieSetter(data) {
  if (data == 'intelligent-solutions')
    intelligent_solutions += 1;
  else if (data == 'intro')
    intro += 1;
  else if (data == 'modules')
    modules += 1;
  else if (data == 'gallery')
    gallery += 1;
  else if (data == 'about-us')
    about_us += 1;
  else
    req_demo += 1;
  var v = document.cookie.match('(^|;) ?' + UserIP + '=([^;]*)(;|$)');
  // return v ? v[2] : null;
  if (v && v[2]) {
    var cookieData = JSON.parse(v[2]);
    cookieData.intelligent_solutions = cookieData.intelligent_solutions + intelligent_solutions;
    cookieData.intro = cookieData.intro + intro;
    cookieData.modules = cookieData.modules + modules;
    cookieData.gallery = cookieData.gallery + gallery;
    cookieData.about_us = cookieData.about_us + about_us;
    cookieData.req_demo = cookieData.req_demo + req_demo;
    document.cookie = UserIP + "=" + JSON.stringify(cookieData);
  } else {
    var cookieData = {};
    cookieData.intelligent_solutions = intelligent_solutions;
    cookieData.intro = intro;
    cookieData.modules = modules;
    cookieData.gallery = gallery;
    cookieData.about_us = about_us;
    cookieData.req_demo = req_demo;
    document.cookie = UserIP + "=" + JSON.stringify(cookieData);
  }

}

$(window).on("beforeunload", function () {
  var v = document.cookie.match('(^|;) ?kpiob12.sid=([^;]*)(;|$)');
  (v && v[2]) ? kpiob12_sid = v[2] : '';
  ipInfo.piob12og9 = kpiob12_sid;
  var cookieData = {
    "intelligent_solutions": intelligent_solutions,
    "intro": intro,
    "modules": modules,
    "gallery": gallery,
    "about_us": about_us,
    "req_demo": req_demo,
    "date": new Date()
  };
  var data = {
    "piob12og9": kpiob12_sid,
    "userIP": UserIP,
    "ipInfo": ipInfo,
    "activity": cookieData
  }
  // $.post("/navigation", data, function (data) { });
})

